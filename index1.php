<?php
  $skillArray[]=array(
    "skillName" => 'Core PHP',
    'covered' => '60',
    'icon' => 'stack-exchange',
  );
  $skillArray[]=array(
    "skillName" => 'Laravel',
    'covered' => '50',
    'icon' => 'weixin',
  );
  $skillArray[]=array(
    "skillName" => 'GIT',
    'covered' => '40',
    'icon' => 'git',
  );
  $skillArray[]=array(
    "skillName" => 'C#',
    'covered' => '40',
    'icon' => 'desktop',
  );
  $skillArray[]=array(
    "skillName" => 'Asp.Net',
    'covered' => '35',
    'icon' => 'joomla',
  );
  
  $skillArray[]=array(
    "skillName" => 'Java',
    'covered' => '30',
    'icon' => 'android',
  );
  $skillArray[]=array(
    "skillName" => 'Javascript',
    'covered' => '60',
    'icon' => 'connectdevelop',
  );
  $skillArray[]=array(
    "skillName" => 'MS SQL',
    'covered' => '60',
    'icon' => 'database',
  );
  $skillArray[]=array(
    "skillName" => 'My SQL',
    'covered' => '40',
    'icon' => 'database',
  );

  $skillArray[]=array(
    "skillName" => 'WordPress',
    'covered' => '60',
    'icon' => 'wordpress',
  );
  $skillArray[]=array(
    "skillName" => 'HubSpot',
    'covered' => '80',
    'icon' => 'github',
  );

  $skillArray[]=array(
    "skillName" => 'Adobe Photoshop',
    'covered' => '50',
    'icon' => 'photo',
  );

  $skillArray[]=array(
    "skillName" => 'API Integration',
    'covered' => '80',
    'icon' => 'wrench',
  );
  $skillArray[]=array(
    "skillName" => 'Salesforce',
    'covered' => '60',
    'icon' => 'user-plus',
  );
  $skillArray[]=array(
    "skillName" => 'Bootstrap',
    'covered' => '80',
    'icon' => 'ticket',
  );
  $skillArray[]=array(
    "skillName" => 'HTML',
    'covered' => '90',
    'icon' => 'html5',
  );
  $skillArray[]=array(
    "skillName" => 'CSS',
    'covered' => '85',
    'icon' => 'css3',
  );
  $skillArray[]=array(
    "skillName" => 'MS Office',
    'covered' => '95',
    'icon' => 'user',
  );
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link
      href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900"
      rel="stylesheet"
    />

    <title>Haris Isani</title>
<!--
Reflux Template
https://templatemo.com/tm-531-reflux
-->
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css" />
    <link rel="stylesheet" href="assets/css/templatemo-style.css" />
    <link rel="stylesheet" href="assets/css/owl.css" />
    <link rel="stylesheet" href="assets/css/lightbox.css" />
  </head>

  <body>
    <div id="page-wraper">
      <!-- Sidebar Menu -->
      <div class="responsive-nav">
        <i class="fa fa-bars" id="menu-toggle"></i>
        <div id="menu" class="menu">
          <i class="fa fa-times" id="menu-close"></i>
          <div class="container">
            <div class="image">
              <a href="#"><img src="assets/images/harisisani.jpg" alt="" /></a>
            </div>
            <div class="author-content">
              <h4>Haris Isani</h4>
              <span>Software Engineer</span>
            </div>
            <nav class="main-nav" role="navigation">
              <ul class="main-menu">
                <li><a href="#section1">About Me</a></li>
                <li><a href="#section2">What I’m good at</a></li>
                <li><a href="#section3">My Work</a></li>
                <li><a href="#section4">Contact Me</a></li>
              </ul>
            </nav>
            <div class="social-network">
              <p>Connect Haris Isani</p>
              <ul class="soial-icons">
                <li>
                  <a target="_blank" href="https://harisisani.clientpoint.net/proposal/unlock-view/proposalId/592671/pin/5123"><i class="fa fa-arrows-alt"></i></a>
                </li>
                <li>
                  <a target="_blank" href="https://www.upwork.com/fl/harisisani"><i class="fa fa-exchange"></i></a>
                </li>
                <li>
                  <a target="_blank" href="https://gitlab.com/harisisani"><i class="fa fa-git"></i></a>
                </li>
                <li>
                  <a target="_blank" href="https://www.youtube.com/channel/UCcHo-NQw4HzNFzdCnZGMJTg"><i class="fa fa-youtube-play"></i></a>
                </li>
                <li>
                  <a target="_blank" href="https://www.linkedin.com/in/haris-isani/"><i class="fa fa-linkedin"></i></a>
                </li>
                <li>
                  <a target="_blank" href="https://www.facebook.com/HARIS.ISANI/"><i class="fa fa-facebook"></i></a>
                </li>
                
              </ul>
            </div>
            <div class="copyright-text">
              <p>Copyright 2022 Haris Isani</p>
            </div>
          </div>
        </div>
      </div>

      <section class="section about-me" data-section="section1">
        <div class="container">
          <div class="section-heading">
            <h2>About Me</h2>
            <div class="line-dec"></div>
            <span>Hello, I am Haris Isani, and I am professionally a Software Engineer.</span>
          </div>
          <div class="left-image-post">
            <div class="row">
              <div class="col-md-6">
                  <iframe style="width: 100%; height: 100%;" src="https://www.youtube.com/embed/eCYoUyH6t0k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
              <div class="col-md-6">
                <div class="right-text">
                  <h4>Synopsis</h4>
                  <p>
                    I have a specialized history working as a Software Developer, Analyst & QA. Some of my previous projects have given me experience in web applications such as Core PHP, Laravel, ASP.Net, HubSpot CMS, WordPress, HTML, CSS, JavaScript etc.
                  </p>
                  <p>I have developed APIs & Integrated a few CRM to my applications via APIs.</p>
                  <p>I explored some of the DevOps platforms like Azure DevOps, Basecamp, Trello, etc.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="right-image-post">
            <div class="row">
              <div class="col-md-6">
                <div class="left-text">
                  <h4>Narrative</h4>
                  <p>These experiences and my adaptable background have given me the ability to design front-end work, create programming logic, derive service system flows, design business brandings, designing business proposals, and much more.</p>
                  <p>My field of study is constituted primarily of a bachelor's degree in Computer Science from a reputable higher education institute.</p>
                  <p>I have completed numerous internships and certifications that not only enabled me to gain important field knowledge but also a way to represent myself on a publicly scaled platform.</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="right-image">
                  <img src="assets/images/haris_isani-min.jpg" alt="" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="section my-services" data-section="section2">
        <div class="container">
          <div class="section-heading">
            <h2>What I’m good at?</h2>
            <div class="line-dec"></div>
            <span>Some of my attained skills are mentioned below:</span>
          </div>
            <div class="row">
              <?php foreach($skillArray as $value){?>
                <div class="col-md-4">
                  <div class="service-item">
                    <h4><span><i class="fa fa-<?=$value["icon"]?>"></span></i>&nbsp;<?php echo $value["skillName"]?></h4>
                    <div class="progress">
                      <div class="progress-bar" role="progressbar" style="width: <?=$value["covered"]?>%;background: #002060;" aria-valuenow="<?=$value["covered"]?>" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>  
                <?php }?>     
            </div>
          </div>
      </section>

      <section class="section my-work" data-section="section3">
        <div class="container">
          <div class="section-heading">
            <h2>My Work</h2>
            <div class="line-dec"></div>
            <span
              >Aenean sollicitudin ex mauris, lobortis lobortis diam euismod sit
              amet. Duis ac elit vulputate, lobortis arcu quis, vehicula
              mauris.</span
            >
          </div>
          <div class="row">
            <div class="isotope-wrapper">
              <form class="isotope-toolbar">
                <label
                  ><input
                    type="radio"
                    data-type="*"
                    checked=""
                    name="isotope-filter"
                  />
                  <span>all</span></label
                >
                <label
                  ><input
                    type="radio"
                    data-type="people"
                    name="isotope-filter"
                  />
                  <span>people</span></label
                >
                <label
                  ><input
                    type="radio"
                    data-type="nature"
                    name="isotope-filter"
                  />
                  <span>nature</span></label
                >
                <label
                  ><input
                    type="radio"
                    data-type="animals"
                    name="isotope-filter"
                  />
                  <span>animals</span></label
                >
              </form>
              <div class="isotope-box">
                <div class="isotope-item" data-type="nature">
                  <figure class="snip1321">
                    <img
                      src="assets/images/portfolio-01.jpg"
                      alt="sq-sample26"
                    />
                    <figcaption>
                      <a
                        href="assets/images/portfolio-01.jpg"
                        data-lightbox="image-1"
                        data-title="Caption"
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>First Title</h4>
                      <span>free to use our template</span>
                    </figcaption>
                  </figure>
                </div>

                <div class="isotope-item" data-type="people">
                  <figure class="snip1321">
                    <img
                      src="assets/images/portfolio-02.jpg"
                      alt="sq-sample26"
                    />
                    <figcaption>
                      <a
                        href="assets/images/portfolio-02.jpg"
                        data-lightbox="image-1"
                        data-title="Caption"
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>Second Title</h4>
                      <span>please tell your friends</span>
                    </figcaption>
                  </figure>
                </div>

                <div class="isotope-item" data-type="animals">
                  <figure class="snip1321">
                    <img
                      src="assets/images/portfolio-03.jpg"
                      alt="sq-sample26"
                    />
                    <figcaption>
                      <a
                        href="assets/images/portfolio-03.jpg"
                        data-lightbox="image-1"
                        data-title="Caption"
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>Item Third</h4>
                      <span>customize anything</span>
                    </figcaption>
                  </figure>
                </div>

                <div class="isotope-item" data-type="people">
                  <figure class="snip1321">
                    <img
                      src="assets/images/portfolio-04.jpg"
                      alt="sq-sample26"
                    />
                    <figcaption>
                      <a
                        href="assets/images/portfolio-04.jpg"
                        data-lightbox="image-1"
                        data-title="Caption"
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>Item Fourth</h4>
                      <span>Re-distribution not allowed</span>
                    </figcaption>
                  </figure>
                </div>

                <div class="isotope-item" data-type="nature">
                  <figure class="snip1321">
                    <img
                      src="assets/images/portfolio-05.jpg"
                      alt="sq-sample26"
                    />
                    <figcaption>
                      <a
                        href="assets/images/portfolio-05.jpg"
                        data-lightbox="image-1"
                        data-title="Caption"
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>Fifth Awesome</h4>
                      <span>Ut sollicitudin risus</span>
                    </figcaption>
                  </figure>
                </div>

                <div class="isotope-item" data-type="animals">
                  <figure class="snip1321">
                    <img
                      src="assets/images/portfolio-06.jpg"
                      alt="sq-sample26"
                    />
                    <figcaption>
                      <a
                        href="assets/images/portfolio-06.jpg"
                        data-lightbox="image-1"
                        data-title="Caption"
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>Awesome Sixth</h4>
                      <span>Donec eget massa ante</span>
                    </figcaption>
                  </figure>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="section contact-me" data-section="section4">
        <div class="container">
          <div class="section-heading">
            <h2>Contact Me</h2>
            <div class="line-dec"></div>
            <span>Feel free to have a discussion on any kind of field-related work.</span>
          </div>
          <div class="row">
            <div class="right-content">
              <div class="container">
                <form id="contact" action="" method="post">
                  <div class="row">
                    <div class="col-md-6">
                      <fieldset>
                        <input
                          name="sender_name"
                          type="text"
                          class="form-control"
                          id="name"
                          placeholder="Your name..."
                          required=""
                        />
                      </fieldset>
                    </div>
                    <div class="col-md-6">
                      <fieldset>
                        <input
                          name="sender_email"
                          type="text"
                          class="form-control"
                          id="email"
                          placeholder="Your email..."
                          required=""
                        />
                      </fieldset>
                    </div>
                    <div class="col-md-12">
                      <fieldset>
                        <input
                          name="subject"
                          type="text"
                          class="form-control"
                          id="subject"
                          placeholder="Subject..."
                          required=""
                        />
                      </fieldset>
                    </div>
                    <div class="col-md-12">
                      <fieldset>
                        <textarea
                          name="message"
                          rows="6"
                          class="form-control"
                          id="message"
                          placeholder="Your message..."
                          required=""
                        ></textarea>
                      </fieldset>
                    </div>
                    <div class="col-md-12">
                      <fieldset>
                        <but onclick="sendResponse();" id="form-submit" class="button">
                          Send Message
                        </but>
                      </fieldset>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>

    <!-- Scripts -->
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="assets/js/isotope.min.js"></script>
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/lightbox.js"></script>
    <script src="assets/js/custom.js"></script>
    <script>
      //according to loftblog tut
      $(".main-menu li:first").addClass("active");

      var showSection = function showSection(section, isAnimate) {
        var direction = section.replace(/#/, ""),
          reqSection = $(".section").filter(
            '[data-section="' + direction + '"]'
          ),
          reqSectionPos = reqSection.offset().top - 0;

        if (isAnimate) {
          $("body, html").animate(
            {
              scrollTop: reqSectionPos
            },
            800
          );
        } else {
          $("body, html").scrollTop(reqSectionPos);
        }
      };

      var checkSection = function checkSection() {
        $(".section").each(function() {
          var $this = $(this),
            topEdge = $this.offset().top - 80,
            bottomEdge = topEdge + $this.height(),
            wScroll = $(window).scrollTop();
          if (topEdge < wScroll && bottomEdge > wScroll) {
            var currentId = $this.data("section"),
              reqLink = $("a").filter("[href*=\\#" + currentId + "]");
            reqLink
              .closest("li")
              .addClass("active")
              .siblings()
              .removeClass("active");
          }
        });
      };

      $(".main-menu").on("click", "a", function(e) {
        e.preventDefault();
        showSection($(this).attr("href"), true);
      });

      $(window).scroll(function() {
        checkSection();
      });
    </script>
    <script>
        function sendResponse(){
          var settings = {
					"url": "./email/sendmail.php",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"Content-Type": "application/json"
					},
					"data": JSON.stringify({
						"sender_email": ""+$('input[name="sender_email"]').val(),
						"sender_name": ""+$('input[name="sender_name"]').val(),
						"subject": ""+$('input[name="subject"]').val(),
						"message": ""+$('#message').val(),
					}),
					};

					$.ajax(settings).done(function (response) {
						console.log(response)
              alert("Response Received");
              $('input[name="sender_email"]').val("");
              $('input[name="sender_name"]').val("");
              $('input[name="subject"]').val("");
              $('#message').val("");
					});

            
        }
    </script>
  </body>
</html>
