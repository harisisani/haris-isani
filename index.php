<?php
  $skillArray[]=array(
    "skillName" => 'Core PHP',
    'covered' => '60',
    'icon' => 'stack-exchange',
  );
  $skillArray[]=array(
    "skillName" => 'Laravel',
    'covered' => '50',
    'icon' => 'weixin',
  );
  $skillArray[]=array(
    "skillName" => 'GIT',
    'covered' => '40',
    'icon' => 'git',
  );
  $skillArray[]=array(
    "skillName" => 'C#',
    'covered' => '40',
    'icon' => 'desktop',
  );
  $skillArray[]=array(
    "skillName" => 'Asp.Net',
    'covered' => '35',
    'icon' => 'joomla',
  );
  
  $skillArray[]=array(
    "skillName" => 'Java',
    'covered' => '30',
    'icon' => 'android',
  );
  $skillArray[]=array(
    "skillName" => 'Javascript',
    'covered' => '60',
    'icon' => 'connectdevelop',
  );
  $skillArray[]=array(
    "skillName" => 'MS SQL',
    'covered' => '60',
    'icon' => 'database',
  );
  $skillArray[]=array(
    "skillName" => 'My SQL',
    'covered' => '40',
    'icon' => 'database',
  );

  $skillArray[]=array(
    "skillName" => 'WordPress',
    'covered' => '60',
    'icon' => 'wordpress',
  );
  $skillArray[]=array(
    "skillName" => 'HubSpot',
    'covered' => '80',
    'icon' => 'github',
  );

  $skillArray[]=array(
    "skillName" => 'Adobe Photoshop',
    'covered' => '50',
    'icon' => 'photo',
  );

  $skillArray[]=array(
    "skillName" => 'API Integration',
    'covered' => '80',
    'icon' => 'wrench',
  );
  $skillArray[]=array(
    "skillName" => 'Salesforce',
    'covered' => '60',
    'icon' => 'user-plus',
  );
  $skillArray[]=array(
    "skillName" => 'Bootstrap',
    'covered' => '80',
    'icon' => 'ticket',
  );
  $skillArray[]=array(
    "skillName" => 'HTML',
    'covered' => '90',
    'icon' => 'html5',
  );
  $skillArray[]=array(
    "skillName" => 'CSS',
    'covered' => '85',
    'icon' => 'css3',
  );
  $skillArray[]=array(
    "skillName" => 'MS Office',
    'covered' => '95',
    'icon' => 'user',
  );
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Haris Isani</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="css/animate.css">
	
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" href="css/magnific-popup.css">
	
	<link rel="stylesheet" href="css/flaticon.css">
	<link rel="stylesheet" href="css/style.css">
</head>
<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
	
	
	<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light site-navbar-target" id="ftco-navbar">
		<div class="container">
			<a class="navbar-brand" href="index.html">Haris Isani<span>.</span></a>
			<button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="oi oi-menu"></span> Menu
			</button>

			<div class="collapse navbar-collapse" id="ftco-nav">
				<ul class="navbar-nav nav ml-auto">
					<li class="nav-item"><a href="#home-section" class="nav-link"><span>Home</span></a></li>
					<li class="nav-item"><a href="#about-section" class="nav-link"><span>About</span></a></li>
					<li class="nav-item"><a href="#skills-section" class="nav-link"><span>Skills</span></a></li>
					<li class="nav-item"><a href="#services-section" class="nav-link"><span>Services</span></a></li>
					<li class="nav-item"><a href="#projects-section" class="nav-link"><span>Projects</span></a></li>
					<li class="nav-item"><a href="#blog-section" class="nav-link"><span>Resume</span></a></li>
					<li class="nav-item"><a href="#contact-section" class="nav-link"><span>Contact</span></a></li>
				</ul>
			</div>
		</div>
	</nav>
	<section id="home-section" class="hero">
		<div class="home-slider owl-carousel">
			<div class="slider-item">
				<div class="overlay"></div>
				<div class="container-fluid px-md-0">
					<div class="row d-md-flex no-gutters slider-text align-items-end justify-content-end" data-scrollax-parent="true">
						<div class="one-third order-md-last img" style="background-image:url(images/bg_harisisani.jpg);">
							<div class="overlay"></div>
							<div class="overlay-1"></div>
						</div>
						<div class="one-forth d-flex  align-items-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
							<div class="text">
								<span class="subheading"><span>Software Engineer & </span>Freelancer</span>
								<h1 class="mb-4 mt-3"><span>Hi, I am </span>Haris Isani</h1>
								<p><a target="_blank" href="https://www.upwork.com/fl/harisisani" class="btn btn-primary">Hire me</a> <a target="_blank" href="./resume/Haris Isani - CV.pdf" class="btn btn-primary btn-outline-primary">Download CV</a></p>
								<style>
									.social-ul{
										list-style-type: none;
										display: flex;
										margin:0px;
										padding:0px;
										font-size: 30px;
									}
									.social-li{
										list-style-type: none;
										padding: 0px 10px 0px 10px;
									}
								</style>
								<p>
									<span class="subheading">Connect</span>
									<div>
										<ul class="social-ul">
											<li class="social-li"><a target="_blank" href="https://harisisani.clientpoint.net/proposal/unlock-view/proposalId/592671/pin/5123"><i class="fa fa-arrows-alt"></i></a></li>
											<li class="social-li"><a target="_blank" href="https://gitlab.com/harisisani"><i class="fa fa-git"></i></a></li>
											<li class="social-li"><a target="_blank" href="https://www.youtube.com/channel/UCcHo-NQw4HzNFzdCnZGMJTg"><i class="fa fa-youtube-play"></i></a></li>
											<li class="social-li"><a target="_blank" href="https://www.linkedin.com/in/haris-isani/"><i class="fa fa-linkedin"></i></a></li>
											<li class="social-li"><a target="_blank" href="https://www.facebook.com/HARIS.ISANI/"><i class="fa fa-facebook"></i></a></li>
										</ul>
									</div>
								</p>
									
							</div>
							
						</div>
					</div>
				</div>
			</div>

			<div class="slider-item">
				<div class="overlay"></div>
				<div class="container-fluid px-md-0">
					<div class="row d-flex no-gutters slider-text align-items-end justify-content-end" data-scrollax-parent="true">
						<div class="one-third order-md-last img" style="background-image:url(images/bg_2_harisisani.jpg);">
							<div class="overlay"></div>
							<div class="overlay-1"></div>
						</div>
						<div class="one-forth d-flex align-items-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
							<div class="text">
								<span class="subheading">Software Engineer & <span>Freelancer</span></span>
								<h1 class="mb-4 mt-3">Hi, I am <span>Haris Isani</span></h1>
								<p><a target="_blank" href="https://www.upwork.com/fl/harisisani" class="btn btn-primary">Hire me</a> <a target="_blank" href="./resume/Haris Isani - CV.pdf" class="btn btn-primary btn-outline-primary">Download CV</a></p>
								<p>
									<span class="subheading">Connect</span>
									<div>
										<ul class="social-ul">
											<li class="social-li"><a target="_blank" href="https://harisisani.clientpoint.net/proposal/unlock-view/proposalId/592671/pin/5123"><i class="fa fa-arrows-alt"></i></a></li>
											<li class="social-li"><a target="_blank" href="https://gitlab.com/harisisani"><i class="fa fa-git"></i></a></li>
											<li class="social-li"><a target="_blank" href="https://www.youtube.com/channel/UCcHo-NQw4HzNFzdCnZGMJTg"><i class="fa fa-youtube-play"></i></a></li>
											<li class="social-li"><a target="_blank" href="https://www.linkedin.com/in/haris-isani/"><i class="fa fa-linkedin"></i></a></li>
											<li class="social-li"><a target="_blank" href="https://www.facebook.com/HARIS.ISANI/"><i class="fa fa-facebook"></i></a></li>
										</ul>
									</div>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="ftco-counter img bg-light" id="section-counter">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 justify-content-center counter-wrap ftco-animate">
					<div class="block-18 d-flex">
						<div class="icon d-flex justify-content-center align-items-center">
							<span class="flaticon-suitcase"></span>
						</div>
						<div class="text">
							<strong class="number" data-number="175">0</strong>
							<span>Project Complete</span>
						</div>
					</div>
				</div>
				<div class="col-lg-3 justify-content-center counter-wrap ftco-animate">
					<div class="block-18 d-flex">
						<div class="icon d-flex justify-content-center align-items-center">
							<span class="flaticon-loyalty"></span>
						</div>
						<div class="text">
							<strong class="number" data-number="350">0</strong>
							<span>Happy Clients</span>
						</div>
					</div>
				</div>
				<div class="col-lg-3 justify-content-center counter-wrap ftco-animate">
					<div class="block-18 d-flex">
						<div class="icon d-flex justify-content-center align-items-center">
							<span class="flaticon-coffee"></span>
						</div>
						<div class="text">
							<strong class="number" data-number="560">0</strong>
							<span>Cups of coffee</span>
						</div>
					</div>
				</div>
				<div class="col-lg-3 justify-content-center counter-wrap ftco-animate">
					<div class="block-18 d-flex">
						<div class="icon d-flex justify-content-center align-items-center">
							<span class="flaticon-calendar"></span>
						</div>
						<div class="text">
							<strong class="number" data-number="4">0</strong>
							<span>Years experienced</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="ftco-about ftco-section ftco-no-pt ftco-no-pb" id="about-section">
		<div class="container">
			<div class="row d-flex no-gutters">
				<div class="col-lg-5 d-flex">
					<div class="img-about img d-flex align-items-stretch" style="height: 80vh;">
						<div class="overlay"></div>
						<div class="img d-flex align-self-stretch align-items-center" style="background-image:url(images/about_haris_isani.jpg);">
						</div>
					</div>
				</div>
				<div class="col-lg-7 pl-md-4 pl-lg-5 py-5">
					<div class="py-md-5">
						<div class="row justify-content-start pb-3">
							<div class="col-md-12 heading-section ftco-animate">
								<span class="subheading">My Intro</span>
								<h2 class="mb-4" style="font-size: 34px; text-transform: capitalize;">About Me</h2>
								<p>Hello, I am Haris Isani, and I am professionally a Software Engineer.</p>
								<p>I have a specialized history working as a Software Developer, Analyst & QA. Some of my previous projects have given me experience in web applications such as Core PHP, Laravel, ASP.Net, HubSpot CMS, WordPress, HTML, CSS, JavaScript etc.</p>
								<p>I have developed APIs & Integrated a few CRM to my applications via APIs.</p>
								<p>I explored some of the DevOps platforms like Azure DevOps, Basecamp, Trello, etc.</p>

								<ul class="about-info mt-4 px-md-0 px-2">
									<li class="d-flex"><span>Name:</span> <span>Haris Isani</span></li>
									<li class="d-flex"><span>Email:</span> <span>harisisani@gmail.com</span></li>
									<li class="d-flex"><span>Phone: </span> <span>+92-344-2329735</span></li>
								</ul>
							</div>
							<div class="col-md-12">
								<div class="my-interest d-lg-flex w-100">
									<div class="interest-wrap d-flex align-items-center">
										<div class="icon d-flex align-items-center justify-content-center">
											<span class="flaticon-listening"></span>
										</div>
										<div class="text">Music</div>
									</div>
									<div class="interest-wrap d-flex align-items-center">
										<div class="icon d-flex align-items-center justify-content-center">
											<span class="flaticon-suitcases"></span>
										</div>
										<div class="text">Travel</div>
									</div>
									<div class="interest-wrap d-flex align-items-center">
										<div class="icon d-flex align-items-center justify-content-center">
											<span class="flaticon-video-player"></span>
										</div>
										<div class="text">Movie</div>
									</div>
									<div class="interest-wrap d-flex align-items-center">
										<div class="icon d-flex align-items-center justify-content-center">
											<span class="flaticon-football"></span>
										</div>
										<div class="text">Sports</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br>
		<br>
		<div class="container">
			<div class="row">		
				<div class="col-lg-5">
						<div class="py-md-5">
							<div class="row justify-content-start pb-3">
								<div class="col-md-12 heading-section ftco-animate">
									<h2 class="mb-4" style="font-size: 34px; text-transform: capitalize;">Narrative</h2>
									<p style="padding:5px;">These experiences and my adaptable background have given me the ability to design front-end work, create programming logic, derive service system flows, design business brandings, designing business proposals, and much more.</p>
									<p style="padding:5px;">My field of study is constituted primarily of a bachelor's degree in Computer Science from a reputable higher education institute.</p>
									<p style="padding:5px;">I have completed numerous internships and certifications that not only enabled me to gain important field knowledge but also a way to represent myself on a publicly scaled platform.</p>
								</div>
						</div>
					</div>
				</div>
			<div class="col-lg-7 d-flex" style="min-height: 40vh;">
				<div class="img-about img d-flex align-items-stretch">
					<div class="overlay"></div>
					<iframe style="width: 100%; height: auto;" src="https://www.youtube.com/embed/eCYoUyH6t0k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</section>
	
	<section class="ftco-section bg-light" id="skills-section">
		<div class="container">
			<div class="row justify-content-center pb-5">
				<div class="col-md-12 heading-section text-center ftco-animate">
					<span class="subheading">Skills</span>
					<h2 class="mb-4">My Attained Skills</h2>
				</div>
			</div>
			<div class="row progress-circle mb-5">
				<?php foreach($skillArray as $value){?>
				<div class="col-lg-4 mb-2">
					<div class="bg-white rounded-lg shadow p-1">
					<h2 class="h5 font-weight-bold text-center mb-4"><?=$value['skillName']?>&nbsp;&nbsp;<i class="fa fa-<?=$value["icon"]?>"></i></h2>

						<!-- Progress bar 1 -->
						<!-- <div class="progress mx-auto" data-value='</?=$value['covered']?>'>
							<span class="progress-left">
								<span class="progress-bar border-primary"></span>
							</span>
							<span class="progress-right">
								<span class="progress-bar border-primary"></span>
							</span>
							<div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
								<div class="h2 font-weight-bold"></?=$value['covered']?><sup class="small">%</sup></div>
							</div>
						</div> -->
						<!-- END -->
					</div>
				</div>
				<?php }?>
			</div>
		</div>
	</section>

	<section class="ftco-section" id="services-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12 heading-section text-center ftco-animate mb-5">
					<span class="subheading">I am grat at</span>
					<h2 class="mb-4">I do awesome services for our clients</h2>
					<p>A few of them are</p>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 col-lg-3">
					<div class="media block-6 services d-block bg-white rounded-lg shadow ftco-animate">
						<div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-3d-design"></span></div>
						<div class="media-body">
							<h3 class="heading mb-3">Web Design</h3>
						</div>
					</div> 
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="media block-6 services d-block bg-white rounded-lg shadow ftco-animate">
						<div class="icon shadow d-flex align-items-center justify-content-center"><span class="flaticon-app-development"></span></div>
						<div class="media-body">
							<h3 class="heading mb-3">Web Application</h3>
							
						</div>
					</div> 
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="media block-6 services d-block bg-white rounded-lg shadow ftco-animate">
						<div class="icon shadow d-flex align-items-center justify-content-center"><span class="flaticon-web-programming"></span></div>
						<div class="media-body">
							<h3 class="heading mb-3">Web Development</h3>
							
						</div>
					</div> 
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="media block-6 services d-block bg-white rounded-lg shadow ftco-animate">
						<div class="icon shadow d-flex align-items-center justify-content-center"><span class="flaticon-branding"></span></div>
						<div class="media-body">
							<h3 class="heading mb-3">API Interation</h3>
						</div>
					</div> 
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 col-lg-3">
					<div class="media block-6 services d-block bg-white rounded-lg shadow ftco-animate">
						<div class="icon shadow d-flex align-items-center justify-content-center"><span class="flaticon-computer"></span></div>
						<div class="media-body">
							<h3 class="heading mb-3">POS Development</h3>
							
						</div>
					</div> 
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="media block-6 services d-block bg-white rounded-lg shadow ftco-animate">
						<div class="icon shadow d-flex align-items-center justify-content-center"><span class="flaticon-vector"></span></div>
						<div class="media-body">
							<h3 class="heading mb-3">CMS Development</h3>
							
						</div>
					</div> 
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="media block-6 services d-block bg-white rounded-lg shadow ftco-animate">
						<div class="icon shadow d-flex align-items-center justify-content-center"><span class="flaticon-vector"></span></div>
						<div class="media-body">
							<h3 class="heading mb-3">Graphic Design</h3>
							
						</div>
					</div> 
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="media block-6 services d-block bg-white rounded-lg shadow ftco-animate">
						<div class="icon shadow d-flex align-items-center justify-content-center"><span class="flaticon-zoom"></span></div>
						<div class="media-body">
							<h3 class="heading mb-3">E-Commerce</h3>
							
						</div>
					</div> 
				</div>
			</div>
		</div>
	</section>


	<section class="ftco-section ftco-project" id="projects-section">
		<div class="container-fluid px-md-4">
			<div class="row justify-content-center pb-5">
				<div class="col-md-12 heading-section text-center ftco-animate">
					<span class="subheading">Accomplishments</span>
					<h2 class="mb-4">I did many awesome projects for my clients</h2>
					<p>A few of them are</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<div class="project img shadow ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(images/projects/POS-Development.png);">
						<div class="overlay"></div>
						<div class="text text-center p-4">
							<h3><a target="_blank" href="http://southlaneanimalhospital.com/south-lane">South-Lane Hospital POS Portal</a></h3>
							<span>POS Software on HTML, CSS, jquery, Bootstrap, PHP</span>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="project img shadow ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(images/projects/Digital-Virtual-Examiner.png);">
						<div class="overlay"></div>
						<div class="text text-center p-4">
							<h3 style="color: #fff;">Digital Virtual Examiner</h3>
							<span>Online Examination Platform on HTML, CSS, JavaScript, Asp.net</span>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="project img shadow ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(images/projects/Customized-Web-Application.png);">
						<div class="overlay"></div>
						<div class="text text-center p-4">
							<h3 style="color: #fff;">Customized Web-Application</h3>
							<span>Custom website on HTML, CSS, jquery, PHP</span>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="project img shadow ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(images/projects/E-Commerce-Website.png);">
						<div class="overlay"></div>
						<div class="text text-center p-4">
							<h3><a target="_blank" href="https://rexbazar.pk/">E-Commerce Website</a></h3>
							<span>WordPress</span>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="project img shadow ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(images/projects/WordPress-Website.png);">
						<div class="overlay"></div>
						<div class="text text-center p-4">
							<h3><a target="_blank" href="https://www.genderhealthtraining.com/">Gender Health's CMS based Website</a></h3>
							<span>WordPress</span>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="project img shadow ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(images/projects/HubSpot-Site.png);">
						<div class="overlay"></div>
						<div class="text text-center p-4">
							<h3><a target="_blank" href="https://info.inspace.chat/product-features">inSpace CMS based Website</a></h3>
							<span>HubSpot</span>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="project img shadow ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(images/projects/Integration.png);">
						<div class="overlay"></div>
						<div class="text text-center p-4">
							<h3><a target="_blank" href="https://www.virustotal.com/">VirusTotal API </a></h3>
							<span>API Integration</span>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="project img shadow ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(images/projects/Hangman.png);">
						<div class="overlay"></div>
						<div class="text text-center p-4">
							<h3><a target="_blank" href="https://youtu.be/vhRmosPEiXg">Hangman Game</a></h3>
							<span>Java</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="ftco-section testimony-section bg-primary">
		<div class="container">
			<div class="row justify-content-center pb-5">
				<div class="col-md-12 heading-section heading-section-white text-center ftco-animate">
					<span class="subheading">Testimonies</span>
					<h2 class="mb-4">What client says about?</h2>
				</div>
			</div>
			<div class="row ftco-animate">
				<div class="col-md-12">
					<div class="carousel-testimony owl-carousel">
						<div class="item">
							<div class="testimony-wrap py-4">
								<div class="text">
									<span class="fa fa-quote-left"></span>
									<p class="lg-4 pl-5">We developed a good partnership. I just wanted to share a quick note and let you know that you guys do a really good job. I’m glad I decided to work with you. You guys are reliable, flexible and very responsive.<br/></p>
									<div class="d-flex align-items-center">
										<!-- <div class="user-img" style="background-image: url(images/person_1.jpg)"></div> -->
										<div class="pl-3">
											<p class="name">Hamza Khan</p>
											<span class="position">IT Specialist</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="testimony-wrap py-4">
								<div class="text">
									<span class="fa fa-quote-left"></span>
									<br class="lg-4 pl-5">Haris is very talented, very skillful, and a problem-solving guy who is always punctual, on time, communicates properly, and always tries to give a solution for a typical or complicated task.</br></br></p>
									<div class="d-flex align-items-center">
										<!-- <div class="user-img" style="background-image: url(images/person_2.jpg)"></div> -->
										<div class="pl-3">
											<p class="name">Syed Turab Naqvi</p>
											<span class="position">Document Processor & Designer</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="testimony-wrap py-4">
								<div class="text">
									<span class="fa fa-quote-left"></span>
									<p class="lg-4 pl-5">I have been in the IT field for the last 10 years. I know Haris for almost 3 years and we have produced some real quality software and websites together. The best thing I found out in Haris is the way he communicates.</p>
									<div class="d-flex align-items-center">
										<!-- <div class="user-img" style="background-image: url(images/person_3.jpg)"></div> -->
										<div class="pl-3">
											<p class="name">Shahid Sheikh</p>
											<span class="position">CMS & SEO Specialist</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="testimony-wrap py-4">
								<div class="text">
									<span class="fa fa-quote-left"></span>
									<p class="lg-4 pl-5">Haris is by far the best developer I've ever had the pleasure of working with.  My favorite project that Haris and I worked on together was for a highly complex pricing tool, involving hundreds of products, equations, and variables.<br/></p>
									<div class="d-flex align-items-center">
										<!-- <div class="user-img" style="background-image: url(images/person_1.jpg)"></div> -->
										<div class="pl-3">
											<p class="name">Kathleen Saenz</p>
											<span class="position">Client-Success & Marketing Specialist</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="testimony-wrap py-4">
								<div class="text">
									<span class="fa fa-quote-left"></span>
									<p class="lg-4 pl-5">Haris has been helping me build a new Wordpress site and clean up an older site, as well as creating graphics for social media campaigns. His work has been stellar and communication excellent. I would highly recommend him!<br/></p>
									<div class="d-flex align-items-center">
										<!-- <div class="user-img" style="background-image: url(images/person_2.jpg)"></div> -->
										<div class="pl-3">
											<p class="name">Shawn V. Giammattei</p>
											<span class="position">Owner & Founder at Gender Health Training Institute</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="testimony-wrap py-4">
								<div class="text">
									<span class="fa fa-quote-left"></span>
									<!-- <p class="lg-4 pl-5">Haris built a POS software & website for my business. He is very fast, talented, skillful & co-operative. He delivered a masterpiece from scratch with very less information we gave I would definitely recommend him!</p> -->
									<p class="lg-4 pl-5">Haris has communicated professionally all the time, he responded all mails in a short amount fo time and was very friendly. He incorporated my feedback without hesitation (more times than agreed in the contract).</p>
									<div class="d-flex align-items-center">
										<!-- <div class="user-img" style="background-image: url(images/person_2.jpg)"></div> -->
										<div class="pl-3">
											<!-- <p class="name">Zeeshan Shauket</p> -->
											<!-- <span class="position">Owner at South-Lane Animal Hospital</span> -->
											<p class="name">Carolina Sirena</p>
											<span class="position">Professional Author</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="ftco-section bg-light" id="blog-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-7 heading-section text-center ftco-animate">
					<span class="subheading">Resume</span>
					<h2 class="mb-4">My Resume</h2>
					<p>Have a fair interest in Programming languages and Computers. I want to work in a collaborative environment where I can make use of my speedy work, professional skills and explore new technologies. I am willing to learn more to grow more in my reputative field.</p>
				</div>
			</div>
			<div class="row d-flex">
				<div class="col-lg-4 d-flex ftco-animate">
					<div class="blog-entry justify-content-end">
						
						<div class="text mt-3 float-right d-block">
							<div class="d-flex align-items-center mb-3 meta">
								<h3 class="mb-4" style="color:#41287B;font-weight:500;">education</h3>
							</div>
							<h3 class="heading">Bachelor of Science Computer Science&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</h3>
							<p><a target="_blank" href="https://iqra.edu.pk/">- Iqra University, Karachi</a></p>
							<h3 class="heading">Intermediate: Computer Science</h3>
							<p>- Govt. College for Men, Karachi</p>
							<h3 class="heading">Matric: Computer Science</h3>
							<p>- S.M Public Academy, Karachi</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 d-flex ftco-animate">
					<div class="blog-entry justify-content-end">
						<div class="text mt-3 float-right d-block">
							<div class="d-flex align-items-center mb-3 meta">
								<h3 class="mb-4" style="color:#41287B;font-weight:500;">experience</h3>
							</div>
							<h3 class="heading">Intern Document Processing & Software Quality Assurance&emsp;&emsp;</h3>
							<p><a target="_blank" href="https://tc-bpo.com/">- Tribe Consulting, Karachi</a></p>
							<h3 class="heading">Technical VA</h3>
							<p><a target="_blank" href="https://www.genderhealthtraining.com/">- Gender Health Training Institute, USA</a></p>
							<h3 class="heading">Freelancer</h3>
							<p><a target="_blank" href="https://www.upwork.com/freelancers/harisisani">- Upwork</a></p>
							<h3 class="heading">Software Developer & Quality Assurance</h3>
							<p><a target="_blank" href="https://codingkey.com/">- CodingKey, Karachi</a></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 d-flex ftco-animate">
					<div class="blog-entry">
						<div class="text mt-3 float-right d-block">
							<div class="d-flex align-items-center mb-3 meta">
								<h3 class="mb-4" style="color:#41287B;font-weight:500;">certifications</h3>
							</div>
							<h3 class="heading">Growth Driven Design, CMS for developers, Email Marketing</h3>
							<p><a target="_blank" href="https://academy.hubspot.com/">- HubSpot Academy</a></p>
							<h3 class="heading">HTML, CSS, WordPress, jQuery, MySql, JSON, GIT, Microsoft Azure</h3>
							<p><a target="_blank" href="https://www.linkedin.com/in/haris-isani/">- LinkedIn Accessment</a></p>
							<h3 class="heading">Frontend Fundamentals</h3>
							<p><a target="_blank" href="https://www.pirple.com/">- Pirple Thinkific</a></p>
							<h3 class="heading">Database Concepts</h3>
							<p><a target="_blank" href="https://www.facebook.com/IUEcompetencia/">- ECOMPENTENCIA</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="ftco-section contact-section ftco-no-pb" id="contact-section">
		<div class="container">
			<div class="row justify-content-center mb-5 pb-3">
				<div class="col-md-7 heading-section text-center ftco-animate">
					<span class="subheading">Contact me</span>
					<h2 class="mb-4">Have a Project?</h2>
					<p>Feel free to Connect</p>
				</div>
			</div>

			<div class="row block-9">
				<div class="col-lg-8">
					<form action="#" class="bg-light p-4 p-md-5 contact-form">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" name="sender_name" placeholder="Your Name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" name="sender_email" class="form-control"  placeholder="Your Email">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" name="subject" class="form-control"  placeholder="Subject">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<textarea cols="30" id="message" rows="7" class="form-control" placeholder="Message"></textarea>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<but onclick="sendResponse();" class="btn btn-primary py-3 px-5">Send Message</but>
								</div>
							</div>
						</div>
					</form>
					
				</div>

				<div class="col-lg-4 d-flex pl-md-5">
					<div class="row">
						<div class="dbox w-100 d-flex">
							<div class="icon d-flex align-items-center justify-content-center">
								<span class="fa fa-map-marker"></span>
							</div>
							<div class="text">
								<p><span>Upwork:</span><a target="_blank" href="https://www.upwork.com/fl/harisisani">Profile Link</a></p>
							</div>
						</div>
						<div class="dbox w-100 d-flex">
							<div class="icon d-flex align-items-center justify-content-center">
								<span class="fa fa-phone"></span>
							</div>
							<div class="text">
								<p><span>Phone:</span> <a href="tel://+92-344-2329735">+92-344-2329735</a></p>
							</div>
						</div>
						<div class="dbox w-100 d-flex">
							<div class="icon d-flex align-items-center justify-content-center">
								<span class="fa fa-paper-plane"></span>
							</div>
							<div class="text">
								<p><span>Email:</span> <a href="mailto:harisisani@gmail.com">harisisani@gmail.com</a></p>
							</div>
						</div>
						<div class="dbox w-100 d-flex">
							<div class="icon d-flex align-items-center justify-content-center">
								<span class="fa fa-globe"></span>
							</div>
							<div class="text">
								<p><span>Website</span> <a href="harisisani.space">harisiani.space</a></p>
							</div>
						</div>
					</div>
					<!-- <div id="map" class="map"></div> -->
				</div>
			</div>
		</div>
	</section>
	

	<footer class="ftco-footer ftco-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://harisisani.space" target="_blank">Haris Isani</a>
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
					</div>
				</div>
			</div>
		</footer>
		
		

		<!-- loader -->
		<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


		<script src="js/jquery.min.js"></script>
		<script src="js/jquery-migrate-3.0.1.min.js"></script>
		<script src="js/popper.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/jquery.waypoints.min.js"></script>
		<script src="js/jquery.stellar.min.js"></script>
		<script src="js/owl.carousel.min.js"></script>
		<script src="js/jquery.magnific-popup.min.js"></script>
		<script src="js/jquery.animateNumber.min.js"></script>
		<script src="js/scrollax.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
		<script src="js/google-map.js"></script>
		
		<script src="js/main.js"></script>
		<script>
			function sendResponse(){
			var settings = {
						"url": "./email/sendmail.php",
						"method": "POST",
						"timeout": 0,
						"headers": {
							"Content-Type": "application/json"
						},
						"data": JSON.stringify({
							"sender_email": ""+$('input[name="sender_email"]').val(),
							"sender_name": ""+$('input[name="sender_name"]').val(),
							"subject": ""+$('input[name="subject"]').val(),
							"message": ""+$('#message').val(),
						}),
						};

						$.ajax(settings).done(function (response) {
							console.log(response)
				alert("Response Received");
				$('input[name="sender_email"]').val("");
				$('input[name="sender_name"]').val("");
				$('input[name="subject"]').val("");
				$('#message').val("");
						});

				
			}
    	</script>
	</body>
	</html>